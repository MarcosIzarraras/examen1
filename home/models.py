from django.db import models
from django.conf import settings
# Create your models here.
class Equipos(models.Model):
	nombre = models.CharField(max_length=256)
	apodo = models.CharField(max_length=256)
	propietario = models.CharField(max_length=256)
	def __str__(self):
		return self.nombre

class Jugadores(models.Model):
	nombre = models.CharField(max_length=256)
	edad = models.IntegerField()
	posicion = models.CharField(max_length=256)
	equipo = models.ForeignKey('Equipos', on_delete=models.CASCADE)
	
	def __str__(self):
		return self.nombre

class Estadios(models.Model):
	nombre = models.CharField(max_length=256)
	propietario = models.CharField(max_length=256)
	superficie = models.CharField(max_length=256)

	def __str__(self):
		return self.nombre