from django.contrib import admin
from .models import Jugadores, Estadios, Equipos
# Register your models here.
@admin.register(Jugadores)
class adminJugadores(admin.ModelAdmin):
	list_display = [
		"id",
		"nombre",
		"edad",
		"posicion",
		"equipo"
	]

@admin.register(Estadios)
class adminEstadios(admin.ModelAdmin):
	list_display = [
		"id",
		"nombre",
		"propietario",
		"superficie"
	]

@admin.register(Equipos)
class adminEquipos(admin.ModelAdmin):
	list_display = [
		"id",
		"nombre",
		"apodo",
		"propietario"
	]