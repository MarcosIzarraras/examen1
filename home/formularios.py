from django import forms
from .models import Jugadores

class FormularioJugador(forms.ModelForm):
	class Meta:
		model = Jugadores
		fields = [
			"equipo"
		]