from django.shortcuts import render
from .models import Equipos, Estadios, Jugadores
from .formularios import FormularioJugador
# Create your views here.

def principal(request):
	return render(request, 'home/principal.html', {})

def listaJugadores(request):
	equipos = Equipos.objects.all()
	queryset = Jugadores.objects.all()
	context = { "lista" : queryset, "equipos" : equipos}
	return render(request, 'home/listaJugadores.html', context)

def listaFiltrada(request):
	equipos = Equipos.objects.all()
	queryset = Jugadores.objects.all()
	posicion = request.GET.get('posicion')
	equipo = request.GET.get('equipo')
	if posicion != "" and equipo != "":	
		queryset = Jugadores.objects.filter(posicion = posicion, equipo= equipo)
	if posicion != "" and equipo == "":	
		queryset = Jugadores.objects.filter(posicion = posicion)
	if posicion == "" and equipo != "":	
		queryset = Jugadores.objects.filter(equipo= equipo)	
	context = { "lista" : queryset, "equipos" : equipos}
	return render(request, 'home/listaJugadores.html', context)

def listaEquipos(request):
	queryset = Equipos.objects.all()
	context = { "lista" : queryset}
	return render(request, 'home/listaEquipos.html', context)

def listaEstadios(request):
	queryset = Estadios.objects.all()
	context = { "lista" : queryset}
	return render(request, 'home/listaEstadios.html', context)

def detallesJugador(request, id):
	queryset = Jugadores.objects.get(id = id)
	context = { "jugador" : queryset}
	return render(request, 'home/detallesJugador.html', context)

def detallesEquipo(request, id):
	queryset = Equipos.objects.get(id = id)
	context = { "equipo" : queryset}
	return render(request, 'home/detallesEquipo.html', context)

def detallesEstadio(request, id):
	queryset = Estadios.objects.get(id = id)
	context = { "estadio" : queryset}
	return render(request, 'home/detallesEstadio.html', context)