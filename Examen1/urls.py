"""Examen1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.principal, name='principal_view'),
    path('listaJugadores/', views.listaJugadores, name='listaJugadores_view'),
    path('listaFiltrada/', views.listaFiltrada, name='listaFiltrada_view'),
    path('listaEquipos/', views.listaEquipos, name='listaEquipos_view'),
    path('listaEstadios/', views.listaEstadios, name='listaEstadios_view'),
    path('detallesEquipo/<int:id>', views.detallesEquipo, name='detallesEquipo_view'),
    path('detallesEstadio/<int:id>', views.detallesEstadio, name='detallesEstadio_view'),
    path('detallesJugador/<int:id>', views.detallesJugador, name='detallesJugador_view'),
]
